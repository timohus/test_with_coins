import itertools


def get_change(change, coins):
    result = []
    exact = list(filter(lambda coin: change % coin == 0 and coin != 1, coins)) if change != 1 else [1]
    if len(exact) > 0:
        result = [max(exact)] * (change//max(exact))
    else:
        largest_coin = min(list(filter(lambda coin: coin <= change, coins)), key=lambda coin: abs(coin - change))
        result.append(largest_coin)

        diff = change - largest_coin

        result = result + (get_change(diff, coins)) if diff > 0 else result.append(diff)
    return result


print(get_change(10, [1, 3, 4, 12]))
print(get_change(10, [1, 3, 4, 5, 12]))
print(get_change(1, [1, 3, 4, 5, 12]))
print(get_change(9, [1, 3, 4, 5, 12]))  # Not good. [3, 3, 3] should be [4, 5]
print(get_change(45, [1, 3, 4, 5, 12]))  # Not good. [5, 5, 5, 5, 5, 5, 5, 5, 5] should be [4, 5, 12, 12, 12]

# Output:
# [4, 3, 3]
# [5, 5]
# [1]
# [3, 3, 3]
# [5, 5, 5, 5, 5, 5, 5, 5, 5]


# Improved version
# Calculates all the possible combinations using itertools.product
# Filters results leaving just most compact one and returns it
def get_change_improved(change, coins):
    variations = [coins_list for i in range(len(coins), 0, -1) for coins_list in itertools.product(coins, repeat=i) if sum(coins_list) == change]
    result = list(map(lambda x: sorted(x), list(filter(lambda x: len(x) == min(map(len, variations)), variations))))
    return [coins for coins in list(set(map(tuple, result)))[0]]


print('\nImproved:')
print(get_change_improved(10, [1, 3, 4, 12]))
print(get_change_improved(10, [1, 3, 4, 5, 12]))
print(get_change_improved(1, [1, 3, 4, 5, 12]))
print(get_change_improved(9, [1, 3, 4, 5, 12]))
print(get_change_improved(45, [1, 3, 4, 5, 12]))

# Output:
# Improved:
# [3, 3, 4]
# [5, 5]
# [1]
# [4, 5]
# [4, 5, 12, 12, 12]
